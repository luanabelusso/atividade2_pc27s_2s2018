/**
 * Exemplo1: Programacao com threads
 * Autor: Luana Belusso
 * Ultima modificacao: 01/04/2018 17:10
 */
package atividade2;

import java.util.Random;

public class Exemplo2  {

    public static void main(String [] args){            
        System.out.println("Inicio da criacao das threads.");

        int id = 1;
        for (int i=1; i<= 20; i++){
            Thread t = new Thread (new PrintTasks("thread"+id, id));
            id++;
            t.start();
        } 

        System.out.println("Threads criadas");
    }    
}
