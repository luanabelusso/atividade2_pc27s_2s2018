/**
 * Exemplo1: Programacao com threads
* Autor: Luana Belusso
* Ultima modificacao: 01/04/2018 17:10
*/
package atividade2;

import java.util.Random;

public class PrintTasks implements Runnable {

    private final int sleepTime; //tempo de adormecimento aleatorio para a thread
    private final String taskName; //nome da tarefa
    private final static Random generator = new Random();
    private int id;    
    private int contador;
    
    public PrintTasks(String name, int ID){
        taskName = name;
        this.id = ID;
        //Tempo aleatorio entre 0 e 1 segundos
        sleepTime = generator.nextInt(1000); //milissegundos
    }
    
    public void run(){
        try{
             if ((this.id % 2) != 0){
                 System.out.printf("Tarefa: %s dorme por %d ms\n", taskName, sleepTime);
                Thread.sleep(sleepTime);
             }else{
                 Thread.yield();
             }
        } catch (InterruptedException ex){
            System.out.printf("%s %s\n", taskName, "terminou de maneira inesperada.");
        }
       System.out.printf("%s acordou!\n", taskName);
    }
       
}
